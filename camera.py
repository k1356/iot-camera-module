import os
import io
import requests

from PIL import Image
from pathlib import Path
from flask import Flask
from flask import send_file

from const import IMG_FILENAME, IMG_HEIGHT, IMG_WIDTH

PARKING_LOT_ID = 1
SEGMENT_ID = 1


def send_image():
    img_dir = f"{Path.home()}/kv6006/{IMG_FILENAME}"
    os.system(f"libcamera-jpeg -n -o {img_dir} --width=1280 --height=720")
    image = Image.open(img_dir)
    image = image.resize((IMG_WIDTH, IMG_HEIGHT), Image.ANTIALIAS)
    with open(img_dir, "rb") as f:
        img_io = f.read()
    url = f"http://192.168.43.3:8000/api/parking_lots/{PARKING_LOT_ID}/segments/{SEGMENT_ID}/count_cars/"
    return requests.post(url, files={"media": img_io})

if __name__ == "__main__":
    response = send_image()
    print(response.text)
